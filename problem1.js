module.exports = function (inventory, id) {
    if (inventory == undefined || !Array.isArray(inventory) || id == undefined) {
        return [];
    }
    else {
        let carObject;
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id === id) {
                carObject = inventory[index];
                return carObject;
            }
        }
        return [];
        
    }
}