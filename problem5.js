module.exports = function(inventory)
{
    if ((arguments.length === 1) && (typeof(inventory) === 'object') && (inventory.length > 20)) {
        let carsBefore2000 = [];
        for (let i = 0; i < inventory.length; i++)
        {
            if (inventory[i] < 2000)
            {
                carsBefore2000.push(inventory[i]);
            }
        }
        return carsBefore2000;
    } else {
        return [];
    }
}