module.exports = function(inventory)
{
    if ((arguments.length === 1) && (typeof(inventory) === 'object') && (inventory.length > 20)) {
        let sortedInventory = inventory.sort((a, b) => a.car_model > b.car_model ? 1 : -1);
        return sortedInventory;
    }
    else {
        return [];
    }
}