module.exports = function(inventory)
{
    if ((arguments.length === 1) && (typeof(inventory) === 'object') && (inventory.length > 20)) {
        let carYears = [];
        for (let i = 0; i < inventory.length; i++)
        {
            carYears.push(inventory[i].car_year);
        }
        return carYears;
    }
    else {
        return [];
    }
}