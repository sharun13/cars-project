module.exports = function(inventory)
{
    if ((arguments.length === 1) && (typeof(inventory) === 'object') && (inventory.length > 20)) {
        let BMWAndAudi = [];
        for (let i = 0; i < inventory.length; i++)
        {
            if ((inventory[i].car_make === 'BMW') || (inventory[i].car_make === 'Audi'))
            {
                BMWAndAudi.push(inventory[i]);
            }
        }
        return BMWAndAudi;
    } else {
        return [];
    }
}