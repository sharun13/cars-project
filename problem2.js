module.exports = function(inventory)
{
    if ((arguments.length === 1) && (typeof(inventory) === 'object') && (inventory.length > 20)) {
        let lastCarIndex = inventory.length - 1;
        let lastCar = inventory[lastCarIndex];
        let lastCarDetails = 'Last car is a ' + lastCar.car_make + ' ' + lastCar.car_model;
        let lastCarObject = [lastCarDetails]
        return lastCarObject;
    }
    else {
        return [];
    }
}